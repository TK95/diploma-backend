import path from 'path';
import Koa from 'koa';
import serve from 'koa-static';
import configureTemplateRendering from 'koa-ejs';
import bodyParser from 'koa-bodyparser';
import socketIO from 'koa-socket';
import co from 'co';
import 'colors';
import {
    requestParamsLogger,
    notFoundErrorHandler,
    simpleCORSForSwagger,
    debugFunctions,
} from './api/middleware';
import logger from './lib/logger';
import rootRouter from './api/root-router';
import rootSocketEventHandler from './api/root-socket-event-handler';


const app = new Koa();
const io = new socketIO();
const port = process.env.PORT || process.env.APP_PORT;

app.use(serve(`${__dirname}/public`));

app.use(bodyParser());

app.use(simpleCORSForSwagger);
app.use(debugFunctions);

app.use(requestParamsLogger);

app.use(rootRouter.routes());
app.use(rootRouter.allowedMethods());

app.use(notFoundErrorHandler);

configureTemplateRendering(app, {
    root: path.join(__dirname, 'public/views'),
    layout: 'home',
    viewExt: 'html',
    cache: false,
    debug: true
});

app.context.render = co.wrap(app.context.render);

io.attach(app);

rootSocketEventHandler(app._io);

app.listen(port, () =>  {
    logger.info('Environment: ' + `${process.env.NODE_ENV }`.yellow);
    logger.info('Server running at ' + `${process.env.PROTOCOL}://localhost:${port}`.magenta)
});

app.on('error', (error, appContext) => {
    logger.error('With context:', appContext);
    logger.error('Application error:', error);
});


export default app;