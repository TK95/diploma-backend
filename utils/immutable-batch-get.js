const immutableBatchGet = (immutableObject, ...fields) => {
    let result = {};
    for ( const field of fields ) {
        result[field] = immutableObject.get(field);
    }
    return result;
};


export default immutableBatchGet;