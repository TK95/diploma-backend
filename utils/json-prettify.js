const JSONPrettify = json => JSON.stringify(json, null, 4);

export default JSONPrettify;