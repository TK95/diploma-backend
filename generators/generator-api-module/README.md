# generator-api-module [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> Generates API module

## Installation

First, install [Yeoman](http://yeoman.io) and generator-api-module using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-api-module
```

Then generate your new project:

```bash
yo api-module
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Denis Ostapenko]()


[npm-image]: https://badge.fury.io/js/generator-api-module.svg
[npm-url]: https://npmjs.org/package/generator-api-module
[travis-image]: https://travis-ci.org/TK95/generator-api-module.svg?branch=master
[travis-url]: https://travis-ci.org/TK95/generator-api-module
[daviddm-image]: https://david-dm.org/TK95/generator-api-module.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/TK95/generator-api-module
[coveralls-image]: https://coveralls.io/repos/TK95/generator-api-module/badge.svg
[coveralls-url]: https://coveralls.io/r/TK95/generator-api-module
