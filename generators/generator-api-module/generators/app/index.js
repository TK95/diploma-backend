'use strict';
const yeoman = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const mkdirp = require('mkdirp');

module.exports = yeoman.Base.extend({
  prompting: function () {
    this.log(yosay(
      'Welcome to the ' + chalk.red('generator-api-module') + ' generator! Using this generator ' +
      'you can easily generate new modules'
    ));

    const prompts = [
      {
        type: 'input',
        name: 'moduleName',
        message: 'What is new module name?',
        default: ''
      }
    ];

    return this.prompt(prompts).then(props => {
      this.props = props;
    });
  },

  writing: function () {
    const newModuleDir = `api/modules/${this.props.moduleName}/`;
    mkdirp.sync(newModuleDir);

    this.props.uppercaseFirstLetter = x => x.substr(0, 1).toUpperCase() + x.substr(1);
    const {moduleName} = this.props;

    this.fs.copyTpl(
      this.templatePath('module/config.js'),
      this.destinationPath(newModuleDir + 'config.js')
    );

    this.fs.copyTpl(
      this.templatePath('module/controller.ejs'),
      this.destinationPath(`${newModuleDir}${moduleName}-controller.js`),
      this.props
    );

    this.fs.copyTpl(
      this.templatePath('module/service.ejs'),
      this.destinationPath(`${newModuleDir}${moduleName}-service.js`),
      this.props
    );

    this.fs.copyTpl(
      this.templatePath('module/dao.ejs'),
      this.destinationPath(`${newModuleDir}${moduleName}-dao.js`),
      this.props
    );

    this.fs.copyTpl(
      this.templatePath('module/routes.ejs'),
      this.destinationPath(`${newModuleDir}routes.js`),
      this.props
    );

    this.fs.copyTpl(
      this.templatePath('module/model.ejs'),
      this.destinationPath(`${newModuleDir}${moduleName}.js`),
      this.props
    );

  }

});
