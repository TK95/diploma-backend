#!/usr/bin/env bash


function initial_configuration() {
    echo "Initial configuration starts. It will require your password"

    init_sequelize_config
    create_logs_directory
    adjust_logs_directory_with_correct_rights
}

function init_sequelize_config() {
    echo "Initializing sequalize config"
    node_modules/sequelize-cli/bin/sequelize init --config config/sequalize.json
}

function create_logs_directory() {
    echo "Creating logs directory"

    local project_name=$(get_project_name)
    sudo mkdir /var/log/${project_name}
}

function adjust_logs_directory_with_correct_rights() {
    echo "Adjusting logs directory with correct rights"

    local project_name=$(get_project_name)
    sudo chmod -R 777 /var/log/${project_name}/
}

function get_project_name() {
    local project_name=$(node -pe "JSON.parse(require('fs').readFileSync('./package.json').toString()).name")
    echo "$project_name"
}


initial_configuration