#!/usr/bin/env bash

path_to_api_documentation="api/documentation"
path_to_swagger_yaml_path="api/swagger/swagger.yaml"

function generate_api_documentation() {
       notify_that_work_started
       generate_swagger_docs
       delete_unnecessary_files
}

function notify_that_work_started() {
    echo "Start generating API documentation. It can take couple of moments"
}

function generate_swagger_docs() {
    swagger-codegen generate -i ${path_to_swagger_yaml_path} -l dynamic-html -o ${path_to_api_documentation}
}

function delete_unnecessary_files() {
    local files_to_delete=(
        ".swagger-codegen-ignore"
        "LICENSE"
        "main.js"
        "package.json"
    )

    for file_name in ${files_to_delete[@]};
        do
           rm -f ${path_to_api_documentation}/${file_name}
        done
}


generate_api_documentation
