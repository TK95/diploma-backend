'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        const {STRING, INTEGER, DATE} = Sequelize;
        return queryInterface.createTable('oauth2-accounts', {
            id: {
                type: INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            accessToken: {
                type: STRING,
                allowNull: false
            },
            name: {
                type: STRING,
                allowNull: false
            },
            userId: {
                type: INTEGER,
                allowNull: false,
                references: {
                    model: 'users',
                    key: 'id'
                },
                onUpdate: 'restrict',
                onDelete: 'cascade'
            },
            createdAt: {
                type: DATE
            },
            updatedAt: {
                type: DATE
            }
        });
    },

    down: function (queryInterface) {
        return queryInterface.dropTable('oauth2-accounts');
    }
};
