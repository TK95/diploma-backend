'use strict';

const GENDER_ENUM = {
    MALE: 'male',
    FEMALE: 'female',
    UNKNOWN: 'unknown'
};

module.exports = {
    up(queryInterface, Sequelize) {
        const {STRING, ENUM, INTEGER, BOOLEAN, DATE} = Sequelize;
        return queryInterface.createTable('users', {
            id: {
                type: INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            email: {
                type: STRING,
                allowNull: false,
                unique: true
            },
            password: {
                type: STRING,
                allowNull: true
            },
            accessToken: {
                type: STRING,
                allowNull: false,
                unique: true
            },
            isActivated: {
                type: BOOLEAN,
                defaultValue: false
            },
            firstName: {
                type: STRING,
                allowNull: true
            },
            lastName: {
                type: STRING,
                allowNull: true
            },
            gender: {
                type: ENUM(GENDER_ENUM.MALE, GENDER_ENUM.FEMALE, GENDER_ENUM.UNKNOWN),
                defaultValue: GENDER_ENUM.UNKNOWN
            },
            createdAt: {
                type: DATE
            },
            updatedAt: {
                type: DATE
            }
        });
    },

    down(queryInterface) {
        return queryInterface.dropTable('users');
    }
};
