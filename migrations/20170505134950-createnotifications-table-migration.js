'use strict';

const STATUS_ENUM = {
  ACTIVE: 'active',
  ARCHIVED: 'archived'
};


module.exports = {
  up: function (queryInterface, Sequelize) {
    const {ENUM, INTEGER, DATE} = Sequelize;
    return queryInterface.createTable('notifications', {
      id: {
        type: INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      cameraId: {
        type: INTEGER,
        allowNull: false,

        references: {
          model: 'cameras',
          key: 'id'
        }
      },
      userId: {
        type: INTEGER,
        allowNull: false
      },
      status: {
        type: ENUM(STATUS_ENUM.ACTIVE, STATUS_ENUM.ARCHIVED),
        defaultValue: STATUS_ENUM.ACTIVE
      },
      createdAt: {
        type: DATE
      },
      updatedAt: {
        type: DATE
      }
    });
  },

  down: function (queryInterface) {
    return queryInterface.dropTable('notifications');
  }
};
