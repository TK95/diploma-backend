'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        const {STRING, INTEGER, DATE} = Sequelize;
        return queryInterface.createTable('cameras', {
            id: {
                type: INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: STRING,
                allowNull: false
            },
            description: {
                type: STRING,
                allowNull: true
            },
            userId: {
                type: INTEGER,
                allowNull: false,
                references: {
                    model: 'users',
                    key: 'id'
                },
                onUpdate: 'restrict',
                onDelete: 'cascade'
            },
            createdAt: {
                type: DATE
            },
            updatedAt: {
                type: DATE
            }
        });
    },

    down: function (queryInterface) {
        return queryInterface.dropTable('cameras');
    }
};
