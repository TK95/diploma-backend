# Koa v2 fp boilerplate
> Advanced boilerplate for creating a web-app with Koa 2.

### Install
You can download this app by doing one of the following: 

1. Use the <kbd>Download Zip</kbd> button at the top right.
2. Run `$ git clone git@github.com:saadq/koa2-skeleton.git` in the command line.

Once you've downloaded the app, run `npm install` to get the necessary dependencies for the project.

### TODO

- Configure project environments <input type="checkbox" disabled/>
- Set up and configure docker <input type="checkbox" disabled/>
- Write more examples <input type="checkbox" disabled/>
- Write tests
