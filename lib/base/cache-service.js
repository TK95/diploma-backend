import redisClient from '../../api/redis-db-context';
import logger from '../../lib/logger';
import 'colors';


export const CACHE_TIME = {
    ONE_MINUTE: 1000 * 60,
    HOUR: 60 * 60 * 1000
};


class CacheService {

    constructor(client) {
        this.redisClient = client;
    }

    get(key) {
        return new Promise((resolve, reject) => {
            return this.redisClient.get(key, (error, value) => error ? reject(error) : resolve(value));
        });
    }

    hgetall(key) {
        return new Promise((resolve, reject) => {
            return this.redisClient.hgetall(key, (error, value) => error ? reject(error) : resolve(value));
        });
    }

    cache(key, value, time = CACHE_TIME.ONE_MINUTE * 5) {
        return new Promise((resolve, reject) => {
            this.redisClient.set(key, value, (error) => {
                if ( error ) {
                    logger.error(error);
                    return reject(error);
                }
                
                this.notifyThatSingleValueWasCached(key, value);
                
                setTimeout(() => {
                    this.redisClient.del(key);
                }, time);

                return resolve();
            });
        });
    }

    cacheHMset(key, ...args) {
        const ttl = args.length % 2 === 0 ? CACHE_TIME.ONE_MINUTE * 5 : args.pop();
        const hmSetCallback = (resolve, reject) => error => {
            if ( error ) {
                logger.error(error);
                return reject(error);
            }

            this.notifyThatPluralValueWasCached(key, args.slice(0, args.length - 1));

            setTimeout(() => {
                this.redisClient.del(key);
            }, ttl);

            return resolve();
        };
        
        
        return new Promise((resolve, reject) => {
            args.push(hmSetCallback(resolve, reject));
            
            return this.redisClient.hmset(key, ...args);
        });
    }
    
    notifyThatSingleValueWasCached(key, value) {
        logger.info(`Cached key` + ` '${key}' `.blue + `with value: ${value}`);
    }

    notifyThatPluralValueWasCached(key, valuesObjectDescriptor) {
        logger.info(`Cached key` + ` '${key}' `.blue + `with value: ${valuesObjectDescriptor}`);
    }

    delete(key) {
        return new Promise((resolve, reject) => 
            this.redisClient.del(key, error =>
                error ? reject(error) : resolve()
            )
        );
    }
    
    getClient() {
        return this.redisClient;
    }

}


export default new CacheService(redisClient);