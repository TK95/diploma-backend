import { Maybe } from 'monet';


export default sequelizeModel => ({

    loadOne(where = {}) {
        return sequelizeModel.findOne({where});
    },

    loadById(id = 0) {
        return sequelizeModel.findById(id);
    },

    load(where = {}) {
        return sequelizeModel.findAll({where});
    },

    create(params = {}) {
        return sequelizeModel.create(params);
    },

    update(params = {}, where = {}) {
        return sequelizeModel.update(params, {where});
    },

    remove(where = {}) {
        return sequelizeModel.destroy({where});
    },

    getModel() {
        return sequelizeModel;
    },
    
    wrapValueIntoMonad(value) {
        return Maybe.Some(value);   
    },

    unwrapMonadValue(monad) {
        return monad.just();
    }

});