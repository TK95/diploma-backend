import {INTERNAL_SERVER_ERROR} from 'http-status-codes';


export default ({
    status = INTERNAL_SERVER_ERROR,
    type = INTERNAL_SERVER_ERROR,
    message
}) => ({
    type, message, status
});