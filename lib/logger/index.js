import winston from 'winston';
const packageInfo = require('../../package.json');


export const logsPath = `/var/log/${packageInfo.name}`;


const getProductionLogger = () => {
    return new (winston.Logger)({
        transports: [
            new (winston.transports.Console)({
                handleExceptions: true,
                timestamp: true,
                colorize: true,
                humanReadableUnhandledException: true
            })
        ],
    });
};

const getDevelopmentLogger = () => {
    return new (winston.Logger)({
        transports: [
            new (winston.transports.Console)({
                humanReadableUnhandledException: true,
                handleExceptions: true,
                colorize: true
            })
        ]
    });
};


const logger = process.env.NODE_ENV === 'development' ? getDevelopmentLogger() : getProductionLogger();


export default logger;