import path from 'path';
import fs from 'fs';
import logger from '../logger';
import DbConfigurerError from './db-configurer-error';


const DB_CONFIG_RELATIVE_PATH = 'config/sequalize.json';

const readConfigFile = () => {
    const pathToConfigurationFile = path.resolve(DB_CONFIG_RELATIVE_PATH);
        
    let dbConfig = null;

    try {
        dbConfig = JSON.parse(fs.readFileSync(pathToConfigurationFile));
    }
    catch (e) {
        logger.error(e);
    }

    return dbConfig;
};

class DbConfigurer {

    static DIALECTS = {
        MY_SQL: 'mysql',
        POSTGRES: 'postgres'
    };

    constructor(configuration) {
        this.configuration = configuration;
        this.env = process.env.NODE_ENV;
    }

    getConfiguration() {
        const confByEnv = { ...this.configuration[this.env] };
        
        if ( ! Object.values(DbConfigurer.DIALECTS).includes(confByEnv.dialect) ) {
            throw new DbConfigurerError('Unsupported dialect');
        }
        
        return confByEnv;
    }

    getAllConfigurations() {
        return this.configuration;
    }
}

export default new DbConfigurer(readConfigFile());