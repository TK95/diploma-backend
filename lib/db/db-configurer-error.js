export default class DbConfigurerError extends Error {
    
    stack = (new Error()).stack;

    message = 'Db configurer base error';

    type = 'Db configurer error';

    constructor(message) {
        super(message);
        this.message = message ? message : this.message;
    }

}