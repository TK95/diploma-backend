import * as R from 'ramda';
import {INTERNAL_SERVER_ERROR} from 'http-status-codes';
import {ApiErrorFactory} from '../factory';
import {JSONPrettify} from '../../utils';
import logger from '../logger';

const logError = error => logger.error(error);

const setResponseBody = (ctx, controllerCallResult) => ctx.body = controllerCallResult;

export const buildErrorResponse = (ctx, error) => {
    ctx.status = error.status || INTERNAL_SERVER_ERROR;
    ctx.body = R.compose(
        ApiErrorFactory
    )(error);
};

const onSuccess = (ctx, next, controllerCallResult) => {
    setResponseBody(ctx, controllerCallResult);
    next();
};

const onError = (ctx, next, error) => {
    buildErrorResponse(ctx, error);
    logError(error);
    next();
};


export default controllerAction => async (ctx, next) => {
    try {
        onSuccess(ctx, next, await controllerAction(ctx, next));
    }
    catch(error) {
        onError(ctx, next, error);
    }
}

