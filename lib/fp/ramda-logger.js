/**
 * Logs params passed to function. Very convenient to use with compose and pipe functions from Ramda lib
 * @param args
 */

import logger from '../logger';


export default (...args) => {
    logger.info('Message from custom log');
    logger.info(args);
    return args;
}