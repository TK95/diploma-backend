import Sequelize from 'sequelize';
import {dbConfigurer} from '../lib/db';

export const defaultModelConfigurations = {
    timestamps: true,
    freezeTableName: true,
    underscored: false
};

const {host, username, password, database, dialect} = dbConfigurer.getConfiguration();


const dbContext = new Sequelize(database, username, password, {
    host,
    dialect,

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },

    dialectOptions: {
        ssl: process.env.NODE_ENV === 'production'
    }

});


export default dbContext;
