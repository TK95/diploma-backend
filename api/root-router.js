import Router from 'koa-router';
import {router as googleRouter} from './modules/google';
import {router as facebookRouter} from './modules/facebook';
import {router as notificationRouter} from './modules/notification';
import {router as cameraRouter} from './modules/camera';
import {router as healthCheckRouter} from './modules/health-check';


const rootRouter = new Router();

rootRouter.get('/', async ctx => {     
    await ctx.render('home')
});

rootRouter.use('/google', googleRouter.routes());
rootRouter.use('/facebook', facebookRouter.routes());
rootRouter.use('/camera', cameraRouter.routes());
rootRouter.use('/health-check', healthCheckRouter.routes());
rootRouter.use('/notification', notificationRouter.routes());


export default rootRouter;