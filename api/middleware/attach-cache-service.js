import cacheService from '../../lib/base/cache-service';


export default async (ctx, next) => {
    ctx.cacheService = cacheService;
    await next();
}