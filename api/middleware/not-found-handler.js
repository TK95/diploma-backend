import { NOT_FOUND } from 'http-status-codes';
import { JSONPrettify } from '../../utils';

export default async ctx => {
  const { request } = ctx;
  if (ctx.status === NOT_FOUND) {
    ctx.body = JSONPrettify({
      type: 'API error',
      status: NOT_FOUND,
      message: `Requested route wasn\'t found. Please check requesting url: ${request.url}. Method: ${request.method}`
    });
  }
};
