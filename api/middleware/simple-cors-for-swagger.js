export default async (ctx, next) => {
    if ( process.env.NODE_ENV === 'development' ) {
        ctx.set('Access-Control-Allow-Origin', '*');
        ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    }
    return await next();
};