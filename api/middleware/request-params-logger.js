import logger from '../../lib/logger';


export default async ({ request }, next) => {
  if (Object.keys(request.query).length > 0 && !request.socket) {
    logger.info('Have query params:');
    logger.info((request.query));
  }
  
  if (Object.keys(request.body).length > 0) {
    logger.info('Have body params:');
    logger.info((request.body));
  }

  await next();
};
