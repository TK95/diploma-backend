import fpLog from '../../lib/fp/ramda-logger';


export default async (ctx, next) => {
  if (process.env.NODE_ENV === 'development') {
    global.fpLog = fpLog;
  }

  await next();
};
