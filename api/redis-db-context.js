import Redis from 'redis';
import logger from '.././lib/logger'
import 'colors';


const host = process.env.REDIS_HOST;
const port = process.env.REDIS_PORT;
const password = process.env.PASSWORD;

const client = Redis.createClient({
    host,
    port,
    password
});

client.on('error', error => {
    logger.error('Unable to connect to Redis', error);
});

client.on('connect', (...args) => {
    logger.info('Redis is connected'.green, args);
});

client.on('ready', (...args) => {
    logger.info(`Redis is ready, host: ${host}, port: ${port}`.green, args);
});


export default client;