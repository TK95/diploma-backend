import {BAD_REQUEST} from 'http-status-codes';


export default class BadRequestError extends Error {
    status = BAD_REQUEST;
    
    stack = (new Error()).stack;
    
    message = 'Bad request error';
    
    type = 'Bad request error';

    constructor(message) {
        super(message);
        this.message = message ? message : this.message;
    }

}