import {UNAUTHORIZED} from 'http-status-codes';

export default class AuthError extends Error {

    status = UNAUTHORIZED;

    stack = (new Error()).stack;

    message = 'Authentication error';

    type = 'Authentication error';

    constructor(message) {
        super(message);
        this.message = message ? message : this.message;
    }

}