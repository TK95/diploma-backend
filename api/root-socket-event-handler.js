import 'colors';
import notificationsSocketEvents from './modules/notification/socket-event-handlers';
import cameraSocketEventHandlers from './modules/camera/socket-event-handlers';


const onConnection = (io, callback) => io.on('connection', socket => {
    logWhoConnected(socket);
    joinSocketToCorrespondingRoom(socket, callback);
});

const logWhoConnected = socket => {
    socket
        .handshake
        .query.isRaspberryPi ?
        console.log('New Pi connected'.blue) :
        console.log('New user connected'.yellow);

    return socket;
};


const joinSocketToCorrespondingRoom = (socket, callback) => {
    const {userId} = socket.handshake.query;
    const roomName = `security-room-${userId}`;
    return socket.join(roomName, error => {
        if ( error ) {
            return console.error(error);
        }
        
        return callback(socket, roomName);
    });
};

const rootSocketEventHandler = io => {
    onConnection(io, (socket, roomName) => {
        notificationsSocketEvents.forEach(registerHandler => registerHandler(socket, roomName));
        cameraSocketEventHandlers.forEach(registerHandler => registerHandler(socket, roomName));
    });
};


export default rootSocketEventHandler;