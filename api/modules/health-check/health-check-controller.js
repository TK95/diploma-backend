import dbContext from '../../db-context';
import logger from '../../../lib/logger';


const doHealthCheck = async ctx => {
    let isAPIalive = true;
    let isDatabaseAlive = true;

    try {
        await dbContext.authenticate()
    }
    catch (error) {
        logger.error(error);
        isDatabaseAlive = false;
    }

    ctx.body = { isAPIalive, isDatabaseAlive  };
};


export default doHealthCheck;