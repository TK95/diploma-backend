import Router from 'koa-router';
import healthCheck from './health-check-controller';


const router = new Router();

router.get('/', healthCheck);


export default router;
