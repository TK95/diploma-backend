import { Maybe } from 'monet';
import Sequelize from '../../db-context';
import baseSequelizeDao from '../../../lib/base/sequelize-dao';
import Notification from './notification';


const composedDAO = baseSequelizeDao(Notification);


export default {
    
  ...composedDAO,

  loadAllNotificationsByUserIdWithCameras(userId) {
      const loadByUserIdWithCamerasSQL = `
        SELECT * FROM (
             SELECT notifications.*,
                     cameras.id AS "cameraId",
                     cameras.name AS "cameraName" 
                     FROM notifications 
                     JOIN cameras
                     ON notifications."cameraId" = cameras.id
          ) notificationsWithCamera
          WHERE  "userId" = 1
      `;
      const queryOptions = { type: Sequelize.QueryTypes.SELECT };
    
      return Sequelize
          .query(loadByUserIdWithCamerasSQL, queryOptions)
          .then(this.wrapValueIntoMonad)
          .then(this._mapNotificationsWithCamera)
          .then(this.unwrapMonadValue)
  },

  _mapNotificationsWithCamera(notificationsWithCamera) {
      const mapper = notificationWithCamera => ({
           id: notificationWithCamera.id,
           status: notificationWithCamera.status,
           createdAt: notificationWithCamera.createdAt,
           camera: {
             id: notificationWithCamera.cameraId,
             name: notificationWithCamera.cameraName
           }
      });
      
      return notificationsWithCamera.flatMap(x => Maybe.Some(x.map(mapper)));
  }

};
