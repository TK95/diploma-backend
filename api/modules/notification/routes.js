import Router from 'koa-router';
import execute from '../../../lib/api/execute-route';
import { load, create, update, remove, archive } from './notification-controller';

const router = new Router();

router.get('/', execute(load));

router.post('/', execute(create));

router.put('/:entityId', execute(update));

router.delete('/:entityId', execute(remove));

router.put('/archive/active-notifications', execute(archive));


export default router;
