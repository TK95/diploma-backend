import * as notificationService from './notification-service';


export const load = ({ request }) => notificationService.load(request.body);

export const create = ({ request }) => notificationService.create({...request.query, ...request.body });

export const update = ({ params, request }) => notificationService.update({
    ...request.query,
    ...request.body,
    entityId: params.entityId
});

export const remove = ({ params }) => notificationService.remove({ id: params.entityId });

export const archive = ({ request }) => notificationService.archive(request.body);