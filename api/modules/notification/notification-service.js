import notificationDao from './notification-dao';
import {STATUS_ENUM} from './notification';


export const load = async({ token }) => notificationDao.loadAllNotificationsByUserIdWithCameras(1);


export const create = params => notificationDao.create(params);


export const update = ({ entityId, ...params }) => notificationDao.update(params, { id: entityId });


export const remove = whereParams => notificationDao.remove(whereParams);


export const archive = ({ notificationsIdsToArchive = [] }) => notificationDao.update(
    {
        status: STATUS_ENUM.ARCHIVED
    },
    {
        id: {
            $in: notificationsIdsToArchive
        }
    }
);
