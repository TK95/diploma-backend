import router from './routes';
import Notification from './notification';


export default {
    router,
    model: Notification
};