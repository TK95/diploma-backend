import {create as createNotification} from './notification-service';
import config from './config';


const onNewNotification = (socket, roomName) => socket.on(config.EVENT_NAMES.NEW_NOTIFICATION, async () => {
    const { userId, cameraId } = socket.handshake.query;
    const newNotification = await createNotification({ userId, cameraId });
    return socket
        .to(roomName)
        .emit(config.EVENT_NAMES.NEW_NOTIFICATION, newNotification);
});


export default [
    onNewNotification
];