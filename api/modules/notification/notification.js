import {STRING, ENUM, INTEGER, BOOLEAN} from 'sequelize';
import sequelize, {defaultModelConfigurations} from '../../db-context';

export const STATUS_ENUM = {
    ACTIVE: 'active',
    ARCHIVED: 'archived'
};

const modelConfiguration = {
    ...defaultModelConfigurations,
    tableName: 'notifications'
};

const Notification = sequelize.define('notification', {
        cameraId: {
            type: INTEGER,

            references: {
                model: 'camera',
                key: 'id'
            }
        },
        userId: {
            type: INTEGER
        },
        status: {
            type: ENUM(STATUS_ENUM.ACTIVE, STATUS_ENUM.ARCHIVED),

            default: STATUS_ENUM.ACTIVE
        }
    },
    modelConfiguration
);



export default Notification;
