export default {

  authMediatorEvent: 'on-facebook-oauth2-authorization-complete',

  oAuth2ServiceName: 'facebook-oauth2',

};
