import * as R from 'ramda';

import Immutable from 'immutable';

import fbGraph from 'fbgraph';
import { authConfig } from '../../../config/';

const { CLIENT_ID, CLIENT_SECRET, REDIRECT_URL, SCOPES } = authConfig.facebook;

export const getOAuthUrl = () => fbGraph.getOauthUrl({
  client_id: CLIENT_ID,
  redirect_uri: REDIRECT_URL,
  scope: SCOPES
});

export const proceedAuth = authenticationCode => R.composeP(
    proceedMainApplicationAuth,
    prepareUserDataForMainApplication,
    getPersonalData,
    setAccessToken,
    authorize
)(authenticationCode);

const authorize = authenticationCode => new Promise((resolve, reject) =>
    fbGraph.authorize({
      client_id: CLIENT_ID,
      redirect_uri: REDIRECT_URL,
      client_secret: CLIENT_SECRET,
      code: authenticationCode
    }, 
        (error, response) => error ? reject(error) : resolve(response)
    )
);


const setAccessToken = ({ access_token: accessToken }) => {
  fbGraph.setAccessToken(accessToken);
  return accessToken;
};

const getPersonalData = () => new Promise((resolve, reject) =>
    fbGraph.get('me', { fields: 'email, picture, gender, name' }, (error, response) =>
        error ? reject(error) : resolve(Immutable.fromJS(response))
    )
);

const prepareUserDataForMainApplication = userData => Immutable.Map({
  firstName: userData.get('name').split(' ')[0],
  lastName: userData.get('name').split(' ')[1],
  gender: userData.get('gender'),
  email: userData.get('email')
});

const proceedMainApplicationAuth = userData => new Promise((resolve, reject) =>
    resolve()
);
