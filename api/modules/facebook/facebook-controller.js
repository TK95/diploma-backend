import * as R from 'ramda';
import * as facebookAuthService from './facebook-auth-service';

export const startAuth = async ctx => R.compose(
    ctx.redirect.bind(ctx),
    facebookAuthService.getOAuthUrl
)();

export const proceedAuth = ({ request }) => facebookAuthService.proceedAuth(request.query.code);
