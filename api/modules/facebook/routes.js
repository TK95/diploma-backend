import Router from 'koa-router';
import { startAuth, proceedAuth } from './facebook-controller';

import execute from '../../../lib/api/execute-route';

const router = new Router();

router.get('/start-auth', startAuth);
router.get('/auth/callback', execute(proceedAuth));

export default router;
