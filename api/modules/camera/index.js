import Camera from './camera';
import router from './routes';


export default {
    model: Camera,
    router
}