import cameraDao from './camera-dao';


export const loadAllByUserId = (token = {}) => cameraDao.loadAllByUserId(token.userId || 1);


export const create = params => cameraDao.create(params);


export const update = ({ entityId, ...params }) => cameraDao.update(params, { id: entityId });


export const remove = whereParams => cameraDao.remove(whereParams);


