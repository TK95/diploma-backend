import {STRING, INTEGER,} from 'sequelize';
import sequelize, {defaultModelConfigurations} from '../../db-context';


const modelConfiguration = {
    ...defaultModelConfigurations,
    tableName: 'cameras'
};

const Camera = sequelize.define('camera', {
        name: {
            type: STRING,
            allowNull: false
        },
        description: {
            type: STRING,
            allowNull: true
        },
        userId: {
            type: INTEGER
        }
    },
    modelConfiguration
);


export default Camera;
