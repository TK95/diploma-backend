import config from './config';


const onTakePhotoRequest = (socket, roomName) => socket.on(config.SOCKET_EVENTS.PHOTOS.MAKE_PHOTO_REQUEST, () => {
    return socket
        .to(roomName)
        .emit(config.SOCKET_EVENTS.PHOTOS.MAKE_PHOTO_REQUEST);
});

const onPhotoIsReady = (socket, roomName) => socket.on(config.SOCKET_EVENTS.PHOTOS.PREVIEW_IS_READY, photo => {
    return socket
        .to(roomName)
        .emit(config.SOCKET_EVENTS.PHOTOS.PREVIEW_IS_READY, photo);
});


const onRecordVideoRequest = (socket, roomName) => socket.on(config.SOCKET_EVENTS.VIDEOS.RECORD_VIDEO_REQUEST, () => {
    return socket
        .to(roomName)
        .emit(config.SOCKET_EVENTS.VIDEOS.RECORD_VIDEO_REQUEST);
});

const onVideoRecorded = (socket, roomName) => socket.on(config.SOCKET_EVENTS.VIDEOS.VIDEO_RECORDED, () => {
    return socket
        .to(roomName)
        .emit(config.SOCKET_EVENTS.VIDEOS.VIDEO_RECORDED);
});


const onDeviceIsBusy = (socket, roomName) => socket.on(config.SOCKET_EVENTS.DEVICE_IS_BUSY, () => {
    return socket
        .to(roomName)
        .emit(config.SOCKET_EVENTS.DEVICE_IS_BUSY);
});


export default [
    onTakePhotoRequest,
    onPhotoIsReady,
    onRecordVideoRequest,
    onVideoRecorded,
    onDeviceIsBusy
];