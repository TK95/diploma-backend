import * as cameraService from './camera-service';


export const load = ({ request }) => cameraService.loadAllByUserId(request.body ? request.body.token : {});

export const create = ({ request }) => cameraService.create({...request.query, ...request.body });

export const update = ({ params, request }) => cameraService.update({
    ...request.query,
    ...request.body,
    entityId: params.entityId
});

export const remove = ({ params }) => cameraService.remove({ id: params.entityId });
