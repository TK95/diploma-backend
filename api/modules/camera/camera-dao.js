import baseSequelizeDao from '../../../lib/base/sequelize-dao';
import Camera from './camera';


const composedDAO = baseSequelizeDao(Camera);


export default {
  ...composedDAO,


  loadAllByUserId(userId) {
      return this.load({ userId })
  }
  
  
};
