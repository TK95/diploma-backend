import google from 'googleapis';
import { authConfig } from '../../../config';

const googleConfig = authConfig.google;
const { CLIENT_ID, CLIENT_SECRET, REDIRECT_URL } = googleConfig;

const OAuth2 = google.auth.OAuth2;

const oauth2Client = new OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);

export default oauth2Client;
