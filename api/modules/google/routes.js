import Router from 'koa-router';
import execute from '../../../lib/api/execute-route';
import { startAuth, proceedAuth } from './google-controller';


const router = new Router();

router.get('/start-auth', startAuth);

router.get('/auth/callback', execute(proceedAuth));

export default router;
