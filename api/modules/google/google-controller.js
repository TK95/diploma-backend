import * as R from 'ramda';
import * as googleAuthService from './google-auth-service';

export const startAuth = async ctx => R.compose(
    authUrl => ctx.redirect(authUrl),
    googleAuthService.getAuthUrl
)();

export const proceedAuth = ctx => googleAuthService.proceedAuth(ctx);
