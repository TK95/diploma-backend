import Immutable from 'immutable';
import * as R from 'ramda';
import * as googlePlusDataProvider from './google-plus-data-provider';


import oauth2Client from './ouath2-client';
import config from './config';

const { GOOGLE_PLUS_ME, OWN_EMAIL } = config.scopes;

const AUTH_SCOPE = [GOOGLE_PLUS_ME, OWN_EMAIL];

export const getAuthUrl = () => oauth2Client.generateAuthUrl({
  access_type: config.localType.OFF_LINE,
  scope: AUTH_SCOPE
});

export const proceedAuth = ctx => R.composeP(
    proceedMainApplicationAuth(ctx),
    prepareUserDataForMainApplication,
    getAccountData,
    setCredentials,
    getAccessTokenByAuthCode,
)(ctx.request.query.code);

const getAccessTokenByAuthCode = code =>
    new Promise((resolve, reject) =>
        oauth2Client.getToken(
            code, (error, tokens) =>
                error ? reject(error) : resolve(tokens)
        )
    );

const setCredentials = (tokens) => {
  oauth2Client.setCredentials(tokens);
  return tokens;
};

const getAccountData = tokens => R.composeP(
    mergeTokensWithUserData(Immutable.fromJS(tokens)),
    getMyAccountData,
)(oauth2Client);

const mergeTokensWithUserData = tokens => userData => userData.set('token', tokens);

const getMyAccountData = oauth2Client => googlePlusDataProvider.getMyAccountData(oauth2Client);

const prepareUserDataForMainApplication = userData => prepareUserDataForAuth(userData);

const prepareUserDataForAuth = userData => Immutable.Map({
  firstName: userData.getIn(['name', 'givenName']),
  lastName: userData.getIn(['name', 'familyName']),
  oauth2Token: {
    value: userData.getIn('token', 'access_token'),
    serviceName: config.oAuth2ServiceName,
  },
  email: userData
        .get('emails')
        .first()
        .get('value'),
});

const proceedMainApplicationAuth = () => userData => new Promise((resolve, reject) => resolve());
