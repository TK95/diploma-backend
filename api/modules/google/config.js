export default {
  scopes: {
    GOOGLE_PLUS_ME: 'https://www.googleapis.com/auth/plus.me',
    OWN_EMAIL: 'https://www.googleapis.com/auth/userinfo.email',
    CALENDAR: 'https://www.googleapis.com/auth/calendar',
  },

  localType: {
    ON_LINE: 'online',
    OFF_LINE: 'offline',
  },

  oAuth2ServiceName: 'google-oauth2',

  authMediatorEvent: 'on-google-oauth2-authorization-complete',

};
