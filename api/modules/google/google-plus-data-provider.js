import * as R from 'ramda';
import Immutable from 'immutable';
import Google from 'googleapis';

const DEFAULT_API_VERSION = 'v1';

const getGooglePlusDataProvider = (version = DEFAULT_API_VERSION) => Google.plus(version);

const googlePlusDataProvider = getGooglePlusDataProvider();

export const getMyAccountData = oauth2Client => R.composeP(
    processMyAccountRequest,
    getMyAccountParams
)(oauth2Client);

const getMyAccountParams = oauth2Client => Promise.resolve({ auth: oauth2Client, userId: 'me' });

const processMyAccountRequest = params =>
    new Promise((resolve, reject) =>
        googlePlusDataProvider.people.get(params, (error, data) =>
            error ?
                reject(error) :
                R.compose(resolve, Immutable.fromJS)(data)
        )
    );
