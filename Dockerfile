FROM node:6.9.5

LABEL maintainer "denis.ostapenko2@gmail.com"

RUN mkdir -p /usr/src/koa2-app

COPY . /usr/src/koa2-app

WORKDIR /usr/src/koa2-app

RUN npm i -g pm2

RUN npm install

EXPOSE 9000

CMD [ "npm", "run", "production"]