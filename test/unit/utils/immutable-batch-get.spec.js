import { expect } from 'chai';
import Immutable from 'immutable';
import { immutableBatchGet } from '../../../utils';

describe('Wrap util', () => {

    it('gets a couple of fields from immutable map', () => {
        const immutableMap = Immutable.Map({
            field1: 'a',
            field2: 'b'
        });
        
        const { field1, field2 } = immutableBatchGet(immutableMap, 'field1', 'field2');

        expect(field1).to.equal(immutableMap.get('field1'));
        expect(field2).to.equal(immutableMap.get('field2'));
    });
});