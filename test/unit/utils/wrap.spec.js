import { expect } from 'chai';
import { wrap } from '../../../utils';

describe('Wrap util', () => {
   
    it('wraps value in function', () => {
        const value = 10;
        const wrappedValue = wrap(value);
        
        expect(typeof wrappedValue === 'function');
    });
    
    it('it should check that unwrapped value is equal to original value', () => {
        const value = 10;
        const wrappedValue = wrap(value);

        expect(wrappedValue()).to.equal(value);
    });
    
});