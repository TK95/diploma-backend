import supertest from 'supertest';
import { expect } from 'chai';
import { model as Camera } from '../../../api/modules/camera';
import app from '../../../app';


const UPDATE_CAMERA_URL = '/camera/1';

describe('Camera module', () => {

    const apiTester = supertest.agent(app.listen());

    const fillDBWithMockData = async () => {
        await Camera.create({
            id: 1,
            name: 'Personal room camera',
            description: 'Watches my room',
            userId: 1
        });
    };

    const clearCamerasTable = async () => {
        await Camera.destroy({ where: {} });
    };

    beforeEach(async () => {
        try {
            await clearCamerasTable();
            await fillDBWithMockData();
        }
        catch (e) {
            console.error(e);
        }
    });

    it('Update camera and checks that fields was updated', done => {
        const requestParams = {
            name: 'CAM002',
            description: 'My new cam',
            userId: 1
        };

        apiTester
            .put(UPDATE_CAMERA_URL)
            .send(requestParams)
            .expect('Content-Type', /json/)
            .expect(200)
            .end(async error => {
                if ( !! error ) {
                    return done(error);
                }
                
                const updateCamera = await Camera.findOne({ where: { id: 1 } });
                
                expect(1).to.equal(updateCamera.id);
                expect(requestParams.name).to.equal(updateCamera.name);
                expect(requestParams.description).to.equal(updateCamera.description);
                expect(requestParams.userId).to.equal(updateCamera.userId);
                
                done();
            });
    });
    

    it('should not fail due to wron request data', done => {
        const requestParams = {};
        apiTester
            .post(UPDATE_CAMERA_URL)
            .send(requestParams)
            .expect('Content-Type', /json/)
            .expect(200)
            .end(done);
    });

});