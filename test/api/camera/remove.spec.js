import supertest from 'supertest';
import { expect } from 'chai';
import { model as Camera } from '../../../api/modules/camera';
import app from '../../../app';


const DELETE_CAMERA_URL = '/camera/1';

describe('Camera module', () => {

    const apiTester = supertest.agent(app.listen());


    const fillDbWithMockData = async () => {
        await Camera.create({
            id: 1,
            name: 'Personal room camera',
            description: 'Watches my room',
            userId: 1
        });
    };

    const clearCamerasTable = async () => {
        await Camera.destroy({ where: {} });
    };

    beforeEach(async () => {
        try {
            await clearCamerasTable();
            await fillDbWithMockData();
        }
        catch (e) {
            console.error(e);
        }
    });

    it('deletes camera with ID = 1', done => {
        apiTester
            .delete(DELETE_CAMERA_URL)
            .expect('Content-Type', /json/)
            .expect(200)
            .end((error, response) => {
                if ( !! error ) {
                    return done(error);
                }

                console.log(response.body);
                done();
            });
    });

});