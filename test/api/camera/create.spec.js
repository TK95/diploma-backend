import supertest from 'supertest';
import { expect } from 'chai';
import { model as Camera } from '../../../api/modules/camera';
import app from '../../../app';


const CREATE_CAMERA_URL = '/camera';

describe('Camera module: create', () => {

    const apiTester = supertest.agent(app.listen());


    const clearCamerasTable = async () => {
         await Camera.destroy({ where: {} });
    };

    beforeEach(async () => {
        try {
            await clearCamerasTable();
        }
        catch (e) {
            console.error(e);
        }
    });

    it('Creates new camera and checks that return payload is match model schema', done => {
        const requestParams = {
            name: 'CAM001',
            description: 'My cam',
            userId: 1
        };

        apiTester
            .post(CREATE_CAMERA_URL)
            .send(requestParams)
            .expect('Content-Type', /json/)
            .expect(200)
            .end((error, response) => {
                if ( !! error ) {
                   return done(error);
                }
                
                const { body: createdCamera } = response;
                
                expect(createdCamera.name).to.equal(requestParams.name);
                expect(createdCamera.description).to.equal(requestParams.description);
                expect(createdCamera.userId).to.equal(requestParams.userId);
                
                done();
            });
    });

    it('Creates new camera and checks that record exists in db', done => {
        const requestParams = {
            name: 'CAM001',
            description: 'My cam',
            userId: 1
        };

        apiTester
            .post(CREATE_CAMERA_URL)
            .send(requestParams)
            .expect('Content-Type', /json/)
            .expect(200)
            .end(async (error, response) => {
                if ( !! error ) {
                    return done(error);
                }
                
                const createdCameraFromDB = await Camera.findOne({ where: { id: response.body.id } });
            
                expect(response.body.id).to.equal(createdCameraFromDB.id);
                expect(requestParams.name).to.equal(createdCameraFromDB.name);
                expect(requestParams.description).to.equal(createdCameraFromDB.description);
                expect(requestParams.userId).to.equal(createdCameraFromDB.userId);

                done();
            });
    });
    
    it('should not create new camera due to validation errors', done => {
        const requestParams = {};
        apiTester
            .post(CREATE_CAMERA_URL)
            .send(requestParams)
            .expect('Content-Type', /json/)
            .expect(500)
            .end(done);
    });

});