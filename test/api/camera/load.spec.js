import supertest from 'supertest';
import { expect } from 'chai';
import { model as User } from '../../../api/modules/user';
import { model as Camera } from '../../../api/modules/camera';
import app from '../../../app';


const LOAD_CAMERAS_URL = '/camera';

describe('Camera module', () => {
    
    const apiTester = supertest.agent(app.listen());

    let user;
    let firstCamera;
    let secondCamera;

    const fillDbWithMockData = async () => {
        user = await User.create({
            id: 1,
            email: 'tk95@me.io',
            password: 'lksdfdslkj3r90ru09hflsdjf30', //assume that it hashed
            accessToken: 'kjsdf239ru2.sf2093ir3290ir2r90jfsd132&$.sdfop',
            isActivated: true,
            firstName: 'Denys',
            lastName: 'Ostapenko'
        });

        firstCamera = await Camera.create({
            id: 1,
            name: 'Personal room camera',
            description: 'Watches my room',
            userId: 1
        });

        secondCamera = await Camera.create({
            id: 2,
            name: 'Parents room camera',
            description: 'Watches parents room',
            userId: 1
        });
        
        await user.save();
        
        await firstCamera.save();
        await secondCamera.save();
    };

    const truncateTables = async () => {
        await User.destroy({ where: {} });
        await Camera.destroy({ where: {} });
    };

    beforeEach(async () => {
        try {
            await truncateTables();
            await fillDbWithMockData();
        }
        catch (e) {
            console.error(e);
        }
    });

    it('loads all cameras', done => {
        apiTester
            .get(LOAD_CAMERAS_URL)
            .expect('Content-Type', /json/)
            .expect(200)
            .end((error, response) => {
                if ( !! error ) {
                   return done(error);
                }   
                
                expect(response.body.length).to.equal(2);
                
                done();
            });
    });

});