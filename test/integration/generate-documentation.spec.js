import fs from 'fs';
import path from 'path';
import { exec } from 'child_process';
import { expect } from 'chai';
import { apiConfig } from '../../config'


const callDocumentationGeneratorScript = callback => {
    const generatorScriptPath = path.join(process.cwd(), 'bin', 'generate_api_documentation.sh');
    const command = `bash ${generatorScriptPath}`;
    return exec(command, callback);
};


describe('Generate documentation suite', () => {

    it('should check that generated documentation is present', done => {
        callDocumentationGeneratorScript(error => {
            if ( error ) {
                return done(error);
            }

            const pathToDocumentations = path.join(process.cwd(), apiConfig.documentationPath, 'docs');
            const isDocumentationIsPresent = fs.existsSync(pathToDocumentations);
            
            expect(isDocumentationIsPresent).to.equal(true);
            
            return done();
        });
    });

});
