import { expect } from 'chai';
import cacheService from '../../lib/base/cache-service';


describe.only('Cache service suite', () => {

    const key = 'some_key';

    beforeEach(async () => {
        await cacheService.delete(key)
    });
    
    it('should get cached value', async () => {
        await cacheService.cache(key, 10, 100);

        const cachedValue = await cacheService.get(key);

        expect(+cachedValue).to.equal(10);
    });

    it('should get multiple cached values', async () => {
        await cacheService.cacheHMset(key, "mjr", "1", "another", "23", "home", 100);
        
        const cachedValue = await cacheService.hgetall(key);
        
        expect(cachedValue).to.eql({ mjr: '1', another: '23', home: '100' });
    });
    
    it('should update key', async () => {
        await cacheService.cache(key, 10, 100);
        await cacheService.cache(key, 11, 100);

        const cachedValue = await cacheService.get(key);

        expect(+cachedValue).to.equal(11);
    });

    it('should delete after when ttl is expired', async () => {
        await cacheService.cache(key, 10, 100);

        return new Promise((resolve, reject) => {
            setTimeout(async () => {
                try {
                    const cachedValue = await cacheService.get(key);

                    expect(cachedValue).to.equal(null);
                   
                    resolve();
                }
                catch (e) {
                    reject(e);
                }
                
            }, 101);
        });
    });

});
