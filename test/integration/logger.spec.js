import fs from 'fs';
import { expect } from 'chai';
import logger from '../../lib/logger';


describe('Logger suite', () => {
    
    logger.on('error', e => {
        console.error(e);
    });

    it('should log some information', () => {

        logger.log('info', 'Some information')

    });

});